<?php

/**
 * Controller
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller managing the user profile.
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends Controller
{

    /**
     * @Route("/espace_membre", name="espace_membre")
     * @Method("GET")
     */
    public function showAction(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $user        = $em->getRepository('AppBundle:User')->findOneById($this->getUser()->getId());
        $access_token = $user->getAccessToken();

        $endpoint1 = 'https://api.instagram.com/v1/users/self/?access_token='.$access_token;
        $endpoint2 = 'https://api.instagram.com/v1/users/self/media/recent/?access_token='.$access_token;

        try{

            $curl1 = curl_init($endpoint1);
            $curl2 = curl_init($endpoint2);

            curl_setopt($curl1, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($curl2, CURLOPT_CONNECTTIMEOUT, 3);

            curl_setopt($curl1, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($curl1, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, false);

            $data1 = json_decode(curl_exec($curl1));
            $data2 = json_decode(curl_exec($curl2));

        } catch (Exception $e){
            die($e->getMessage());
        }

        if($data1->meta->code == 200 && $data2->meta->code == 200){
            //dump($data->data);
            $follows = $data1->data->counts->follows ;
            $followed_by = $data1->data->counts->followed_by;
            $medias = $data1->data->counts->media;

            $urlimage = $data2->data;
           // dump($urlimage);
            $output = array();
            foreach ($urlimage as $url){
                if ($url->type === 'image'){
                   $output[] = array(
                       $url->images->standard_resolution->url,
                       $url->likes->count,
                       $url->comments->count
                       );
                }
            }
            return $this->render('Profile/show.html.twig', array(
                'user'          => $user,
                'follow'        => $follows,
                'followed_by'   => $followed_by,
                'media'         =>$medias,
                'urlimage'       =>$output
            ));
        }
        else {
            return $this->render('Profile/show1.html.twig', array(
                'user'          => $user
            ));
        }
    }
}
