<?php

/**
 * Created by PhpStorm.
 * User: agne
 * Date: 8/29/18
 * Time: 10:04 AM
 */
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraint as Assert;
use Symfony\Component\Validator\Constraints\Date;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="gender", nullable=true)
     */
    private $gender;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstname;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastname;


    /**
     * @ORM\Column(type="string", length=55, nullable=true)
     */
    protected $accessToken;
    /**
     * @ORM\Column(type="string", length=55, nullable=true)
     */
    protected $facebookId;
    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $instagramId;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageName;
    /**
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="imageName")
     * @var File
     */

    private $imageFile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $address;


    protected $isSign = 0;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $country;
    /**
     * @var string
     * @ORM\Column(type="date", name="birth_date", nullable=true)

     */
    private $birthDate;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $updatedAt;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param mixed $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * @return mixed
     */
    public function getInstagramId()
    {
        return $this->instagramId;
    }

    /**
     * @param mixed $instagramId
     */
    public function setInstagramId($instagramId)
    {
        $this->instagramId = $instagramId;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param mixed $imageName
     * @return User
     */
    public function setImageName($imageName)
    {
        /*if($imageName){
            $this->imageName = '/img/membres/'.$imageName;
        } else {
            $this->imageName = null;
        }

        return $this;*/
        $this->imageName = $imageName;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }
    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getIsSign()
    {
        return $this->isSign;
    }

    /**
     * @param int $isSign
     */
    public function setIsSign($isSign)
    {
        $this->isSign = $isSign;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Set birthDate
     *
     * @param date $birthDate
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return date
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }





}